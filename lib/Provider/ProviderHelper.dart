import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Counter extends ChangeNotifier {
  int count = 0;

  void incrememnt() {
    count++;
    notifyListeners();
  }
}

class Name {
  String? name;

  Name({this.name});
}

class FinalData {
  String? name;
  int? count;

  FinalData({this.name, this.count});

  response() {
    return '$name of count $count';
  }
}

class FutureData {
  String? data;

  FutureData({this.data});
}

class StreamData {
  String? data;

  StreamData({this.data});
}

Future<FutureData> getData() async {
  await Future.delayed(Duration(seconds: 5));
  return FutureData(data: 'Welcome !');
}
