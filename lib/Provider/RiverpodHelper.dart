import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:sample/Provider/ProviderHelper.dart';

final greetingText = Provider((ref) => 'Welcome');

final firstString = Provider((ref) => 'First');
final secondString = Provider((ref) => 'Second');

final incrementProvider = ChangeNotifierProvider((ref) => Counter());

final fakeHttpClient = Provider((ref) => FakeHttp());

final responseProv = FutureProvider<String>(
  (ref) async {
    final httpClient = ref.read(fakeHttpClient);
    return httpClient.getData("www.youtube.com");
  },
);

final responseProvFamily = FutureProvider.autoDispose.family<String, String>(
  (ref, url) async {
    final httpClient = ref.read(fakeHttpClient);
    return httpClient.getData(url);
  },
);

class FakeHttp {
  Future<String> getData(String url) async {
    await Future.delayed(Duration(seconds: 5));
    return 'Data from $url';
  }
}

final counterStateProvider = StateProvider<int>((ref) => 0);

class Clock extends StateNotifier<DateTime> {
  Clock() : super(DateTime.now()) {
    timer = Timer.periodic(
      Duration(seconds: 0),
      (timer) => state = DateTime.now(),
    );
  }

  late final Timer timer;

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }
}

final clockProvider = StateNotifierProvider((ref) => Clock());
