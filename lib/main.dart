import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:provider/provider.dart';
// import 'package:sample/Provider/ProviderHelper.dart';
// import 'package:sample/Screens/CounterProgram.dart';
import 'package:sample/Screens/RiverpodProgram.dart';

////////////////////////////////////////////////////////////////////////////////
/// Provider
////////////////////////////////////////////////////////////////////////////////

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MultiProvider(
//       providers: [
//         ChangeNotifierProvider<Counter>(create: (_) => Counter()),
//         Provider<Name>(create: (context) => Name(name: "Alpha")),
//         ProxyProvider2<Counter, Name, FinalData>(
//           update: (context, counter, user, _) => FinalData(
//             name: user.name,
//             count: counter.count,
//           ),
//         ),
//         FutureProvider<FutureData>(
//           create: (_) => getData(),
//           initialData: FutureData(data: 'Loading...'),
//         ),
//       ],
//       child: MaterialApp(
//         title: 'Flutter Demo',
//         debugShowCheckedModeBanner: false,
//         theme: ThemeData(primarySwatch: Colors.blue),
//         home: CounterProgram(),
//       ),
//     );
//   }
// }

////////////////////////////////////////////////////////////////////////////////
/// Flutter_Riverpod
////////////////////////////////////////////////////////////////////////////////

void main() {
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: RiverpodProgram(),
    );
  }
}
