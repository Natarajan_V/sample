import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample/Provider/ProviderHelper.dart';

class CounterProgram extends StatelessWidget {
  const CounterProgram({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Counter>(context, listen: false);
    //final data = Provider.of<FinalData>(context);

    final futureData = Provider.of<FutureData>(context);

    print('Loaded');

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Provider Demo'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("${futureData.data}"),
            Consumer<FinalData>(
              builder: (_, data, __) => Text(
                "${data.response()}",
              ),
            ),
          ],
        ),
      ),
      //body: Center(child: Text("${data.response()}")),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: counter.incrememnt,
      ),
    );
  }
}
