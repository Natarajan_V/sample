import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:sample/Provider/RiverpodHelper.dart';

////////////////////////////////////////////////////////////////////////////////
/// ConsumerWidget // reloads everything inside return
////////////////////////////////////////////////////////////////////////////////

// class RiverpodProgram extends ConsumerWidget {
//   const RiverpodProgram({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context, ScopedReader watch) {
//     final greet = watch(greetingText);
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text('Riverpod Demo'),
//       ),
//       body: Center(child: Text(greet)),
//     );
//   }
// }

////////////////////////////////////////////////////////////////////////////////
/// Consumer with StatelessWidget // only reloads where needed
////////////////////////////////////////////////////////////////////////////////

// class RiverpodProgram extends StatelessWidget {
//   const RiverpodProgram({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text('Riverpod Demo'),
//       ),
//       body: Center(
//         child: Consumer(
//           builder: (context, watch, child) {
//             return Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Text(watch(greetingText)),
//                 Text(watch(firstString)),
//                 Text(watch(secondString)),
//               ],
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

////////////////////////////////////////////////////////////////////////////////
/// without watch // Context.read
////////////////////////////////////////////////////////////////////////////////

// class RiverpodProgram extends StatelessWidget {
//   const RiverpodProgram({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text('Riverpod Demo'),
//       ),
//       body: Center(
//         child: Consumer(
//           builder: (context, watch, child) {
//             final incProv = watch(incrementProvider);
//             return Text('${incProv.count}');
//           },
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {
//           context.read(incrementProvider).incrememnt();
//         },
//         child: Icon(Icons.add),
//       ),
//     );
//   }
// }

////////////////////////////////////////////////////////////////////////////////
/// FutureProvider
////////////////////////////////////////////////////////////////////////////////

// class RiverpodProgram extends StatelessWidget {
//   const RiverpodProgram({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text('Riverpod Demo'),
//       ),
//       body: Center(
//         child: Consumer(
//           builder: (context, watch, child) {
//             final asyncValue = watch(responseProv);
//             return asyncValue.map(
//               data: (data) => Text(data.value),
//               loading: (_) => CircularProgressIndicator(),
//               error: (e) => Text(e.error.toString()),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

////////////////////////////////////////////////////////////////////////////////
/// FutureProvider with Family
////////////////////////////////////////////////////////////////////////////////

// class RiverpodProgram extends StatelessWidget {
//   const RiverpodProgram({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text('Riverpod Demo'),
//       ),
//       body: Center(
//         child: Consumer(
//           builder: (context, watch, child) {
//             final asyncValue = watch(responseProvFamily('www.google.com'));
//             return asyncValue.map(
//               data: (data) => Text(data.value),
//               loading: (_) => CircularProgressIndicator(),
//               error: (e) => Text(e.error.toString()),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

////////////////////////////////////////////////////////////////////////////////
/// StateProvider and Provider listener
////////////////////////////////////////////////////////////////////////////////

class RiverpodProgram extends StatelessWidget {
  const RiverpodProgram({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProviderListener<StateController>(
      provider: counterStateProvider,
      onChange: (context, counterState) =>
          ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Value is ${counterState.state}'),
        ),
      ),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Riverpod Demo'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Consumer(
                builder: (context, watch, child) {
                  final incProv = watch(counterStateProvider);
                  print("Increment Called");
                  return Text(
                    '${incProv.state}',
                    style: TextStyle(fontSize: 20),
                  );
                },
              ),
              Consumer(
                builder: (context, watch, child) {
                  final clock = watch(clockProvider);
                  String time = DateFormat.Hms().format(clock);

                  return Text('$time', style: TextStyle(fontSize: 20));
                },
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            context.read(counterStateProvider).state++;
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
